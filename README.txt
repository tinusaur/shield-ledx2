Shield-LEDx2 - Tinusaur Shield-LEDx2 Project

-----------------------------------------------------------------------------------
 Copyright (c) 2017 Neven Boyanov, The Tinusaur Team. All Rights Reserved.
 Distributed as open source software under MIT License, see LICENSE.txt file.
 Retain in your source code the link http://tinusaur.org to the Tinusaur project.
-----------------------------------------------------------------------------------

This is code for the Tinusaur Shield-LEDx2 project.

Folders and modules:
- shield_ledx2         - Library
- shield_ledx2_blink1  - 1 blinking LED
- shield_ledx2_blink2  - 2 blinking LEDs
- shield_ledx2_fade1   - 1 fading in/out LED
- shield_ledx2_fade2   - 2 fading in/out LEDs
- shield_ledx2_util    - Utilities, images, photos, etc.

This was developed for and tested on the ATtiny85 microcontroller.

==== Links ====

Official Tinusaur Project website: http://tinusaur.org
Project Shield-LEDx2 page: https://tinusaur.org/products/shields/tinusaur-shield-ledx2/
Project Shield-LEDx2 source code: https://bitbucket.org/tinusaur/shield-ledx2

Twitter: https://twitter.com/tinusaur
Facebook: https://www.facebook.com/tinusaur

