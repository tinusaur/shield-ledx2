/**
 * The Shield-LEDx2 Project
 *
 * @created 2017-03-14
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Shield-LEDx2 project.
 *
 * Copyright (c) 2017 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/shield-ledx2
 *
 */

// ----------------------------------------------------------------------------

#ifndef SHIELDLEDX2_H
#define SHIELDLEDX2_H

// ----------------------------------------------------------------------------

#include <stdint.h>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Shield-LEDx2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//               Shield LEDx2
//                  +-------+
//      (RST)-->    +   Vcc +---(+)--VCC--
// --[OWOWOD]--> +--+   PB2 +-------------
// --------------+ PB4  PB1 +---LED2------ (GREEN)
// --------(-)---+ GND  PB0 +---LED1------ (RED)
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Ports definitions

#define SHIELD_LEDX2_LED1 PB0	// Define the LED1 (RED) I/O port
#define SHIELD_LEDX2_LED2 PB1	// Define the LED2 (GREEN) I/O port

// ----------------------------------------------------------------------------

#endif

// ----------------------------------------------------------------------------
