/**
 * Shield-LEDx2 - Test
 *
 * @created 2017-03-14
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Shield-LEDx2 project.
 *
 * Copyright (c) 2017 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/shield-ledx2
 *
 */

// ============================================================================

// #define F_CPU 1000000UL
// NOTE: The F_CPU (CPU frequency) should not be defined in the source code.
//       It should be defined in either (1) Makefile; or (2) in the IDE. 

#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <util/delay.h>

#include "shield_ledx2/shield_ledx2.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Shield-LEDx2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//               Shield LEDx2
//                  +-------+
//      (RST)-->    +   Vcc +---(+)--VCC--
// --[OWOWOD]--> +--+   PB2 +-------------
// --------------+ PB4  PB1 +---LED2------ (GREEN)
// --------(-)---+ GND  PB0 +---LED1------ (RED)
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//	Duty cycle and the Pulse-width modulation
//
//	Pulse-width modulation with Duty cycle of 50%, sample:
//	50% ____|¯¯¯¯|____|¯¯¯¯|_____
//
//	Pulse-width modulation for the fade-in and fade-out effect, sample:
//	10% _____|¯|_________|¯|_____
//	30% ____|¯¯¯|_______|¯¯¯|____
//	50% ___|¯¯¯¯¯|_____|¯¯¯¯¯|___
//	70% __|¯¯¯¯¯¯¯|___|¯¯¯¯¯¯¯|__
//	90% _|¯¯¯¯¯¯¯¯¯|_|¯¯¯¯¯¯¯¯¯|_
//	70% __|¯¯¯¯¯¯¯|___|¯¯¯¯¯¯¯|__
//	50% ___|¯¯¯¯¯|_____|¯¯¯¯¯|___
//	30% ____|¯¯¯|_______|¯¯¯|____
//	10% _____|¯|_________|¯|_____
//
//	REF: https://en.wikipedia.org/wiki/Duty_cycle
//	REF: https://en.wikipedia.org/wiki/Pulse-width_modulation

// ----------------------------------------------------------------------------

#define DUTYCYCLE_MAX 600
#define DUTYCYCLE_MIN 1

int main(void) {

	// ---- Initialization ----
	
	// Set the LED port(s) as output.
	DDRB |= (1 << SHIELD_LEDX2_LED1);
	DDRB |= (1 << SHIELD_LEDX2_LED2);
	
	// ---- Testing: Fade in/out 2 LEDs ----
	
	uint8_t fade_out = 0;
	uint16_t dutycycle = DUTYCYCLE_MIN;

	for (;;) { // Infinite main loop
		PORTB |= (1 << SHIELD_LEDX2_LED1);
		PORTB &= ~(1 << SHIELD_LEDX2_LED2);
		_delay_loop_2(dutycycle);

		PORTB &= ~(1 << SHIELD_LEDX2_LED1);
		PORTB |= (1 << SHIELD_LEDX2_LED2);
		_delay_loop_2(DUTYCYCLE_MAX - dutycycle);
		
		if (fade_out == 0) {
			dutycycle++;
			if (dutycycle >= DUTYCYCLE_MAX - 1) fade_out = 1;
		} else {
			dutycycle--;
			if (dutycycle <= DUTYCYCLE_MIN) fade_out = 0;
		}
	}

	// Return the mandatory for the "main" function int value. It is "0" for success.
	return 0;
}

// ============================================================================
